package net.guides.springboot2.crud.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

//@org.springframework.web.bind.annotation.ControllerAdvice
//@Component
//@Target(value={TYPE})
//@Retention(value=RUNTIME)
//@Documented
//Specialization of @Component for classes that declare @ExceptionHandler, @InitBinder, or @ModelAttribute methods to be shared across multiple @Controller classes.
@ControllerAdvice
public class GlobalExceptionHandler {
	// @org.springframework.web.bind.annotation.ExceptionHandler
	// Annotation for handling exceptions in specific handler classes and/or handler methods.
	@ExceptionHandler(ResourceNotFoundException.class) 
	
	// org.springframework.http.ResponseEntity<?>
	// Extension of HttpEntity that adds a HttpStatus status code.Used in RestTemplate as well @Controller methods.
	// In RestTemplate, this class is returned by getForEntity() and exchange() 
	public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request){
		
		// WebRequest.getDescription(boolean includeClientInfo)
		// Get a short description of this request,typically containing request URI and session id.
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		
		// org.springframework.http.ResponseEntity.ResponseEntity<ErrorDetails>(@Nullable ErrorDetails body, HttpStatus status)
		// Create a new ResponseEntity with the given body and status code, and no headers.
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> globalExceptionHandler(Exception ex, WebRequest request){
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getLocalizedMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
