package net.guides.springboot2.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.guides.springboot2.crud.model.Tag;

public interface TagRepository extends JpaRepository<Tag, Long>  {

}
