package net.guides.springboot2.crud.controller;

import java.util.List;
//import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import net.guides.springboot2.crud.model.Comment;
import net.guides.springboot2.crud.model.Post;
import net.guides.springboot2.crud.repository.PostRepository;

@RestController
@RequestMapping("/api/v1")
public class PostController {
	
	@Autowired
	PostRepository postRepository;
	
	@GetMapping("/posts")
	public List<Post> getAllPosts(){
		return postRepository.findAll();
	}
	
	@PostMapping("/posts")
	public Post createPost(@Valid @RequestBody Post post) {
		return postRepository.save(post);
	}
}
