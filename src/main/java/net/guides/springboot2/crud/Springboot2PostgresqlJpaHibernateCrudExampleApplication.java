package net.guides.springboot2.crud;


import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.guides.springboot2.crud.model.Post;
import net.guides.springboot2.crud.model.PostTag;
import net.guides.springboot2.crud.model.Tag;
import net.guides.springboot2.crud.model.Comment;
import net.guides.springboot2.crud.repository.PostRepository;
import net.guides.springboot2.crud.repository.PostTagRepository;
import net.guides.springboot2.crud.repository.TagRepository;

@SpringBootApplication
public class Springboot2PostgresqlJpaHibernateCrudExampleApplication//{ 
implements CommandLineRunner{ // CommandLineRunner를 통해서 Post와 Comment에 데이터를 삽입하는 경우, 기동 시 작동함.

	public static void main(String[] args) {
		SpringApplication.run(Springboot2PostgresqlJpaHibernateCrudExampleApplication.class, args);
	}

	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private PostTagRepository postTagRepository;
	
	@Override
	public void run(String...args) throws Exception{
		String localNow = LocalDateTime.now().toString();
		
		Post post = new Post("[RUN]one to manay" + localNow, "[RUN]one to many mapping using JPA and hibernate" + localNow);
		Comment comment1 = new Comment("[RUN]Very useful" + localNow);
		Comment comment2 = new Comment("[RUN]Informative" + localNow);
		Comment comment3 = new Comment("[RUN]Great post" + localNow);
		post.getComments().add(comment1);
		post.getComments().add(comment2);
		post.getComments().add(comment3);
		
		Tag tag = new Tag("[RUN]MOVIE" + localNow);
		
		PostTag postTag = new PostTag();
		postTag.setPost(post);
		postTag.setTag(tag);
		postTag.setUpdAt(LocalDateTime.now());
		
		this.tagRepository.save(tag);
		this.postRepository.save(post);
		this.postTagRepository.save(postTag);
	}
}
