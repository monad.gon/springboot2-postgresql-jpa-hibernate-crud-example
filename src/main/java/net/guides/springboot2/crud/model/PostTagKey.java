package net.guides.springboot2.crud.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/*
	Note, that there're some key requirements, which a composite key class has to fulfill:
	We have to mark it with @Embeddable
	It has to implement java.io.Serializable
	We need to provide an implementation of the hashcode() and equals() methods
	None of the fields can be an entity themselves
*/

// NOT USING ----  https://www.baeldung.com/jpa-many-to-many

@Embeddable
public class PostTagKey implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name = "post_id")
	Long postId;
	
	@Column(name = "tag_id")
	Long tagId;

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((postId == null) ? 0 : postId.hashCode());
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostTagKey other = (PostTagKey) obj;
		if (postId == null) {
			if (other.postId != null)
				return false;
		} else if (!postId.equals(other.postId))
			return false;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		return true;
	}
	
}
