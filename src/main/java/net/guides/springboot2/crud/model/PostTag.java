package net.guides.springboot2.crud.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name="post_tags")
public class PostTag {
//	@EmbeddedId
//	PostTagKey id;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	//@MapsId("post_id") // PostTagKey 사용시
	@JoinColumn(name="post_id")
	private Post post;
	
	@ManyToOne
	//@MapsId("tag_id") // PostTagKey 사용시
	@JoinColumn(name="tag_id")
	private Tag tag;

	@Column(name = "upd_at")
	private LocalDateTime updAt;
	
	public PostTag() {
		
	}
	
//	public PostTagKey getId() {
//		return id;
//	}
//
//	public void setId(PostTagKey id) {
//		this.id = id;
//	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public LocalDateTime getUpdAt() {
		return updAt;
	}

	public void setUpdAt(LocalDateTime updAt) {
		this.updAt = updAt;
	}
}
