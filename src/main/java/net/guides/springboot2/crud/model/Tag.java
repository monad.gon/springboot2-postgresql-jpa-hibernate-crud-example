package net.guides.springboot2.crud.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tag")
public class Tag {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(mappedBy = "post")
	private Set<PostTag> postTag;

	public Tag(){
		
	}
	
	public Tag(String name){
		this.name = name;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<PostTag> getPostTag() {
		return postTag;
	}

	public void setPostTag(Set<PostTag> postTag) {
		this.postTag = postTag;
	}
	
}
