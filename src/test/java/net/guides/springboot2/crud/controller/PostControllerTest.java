package net.guides.springboot2.crud.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.guides.springboot2.crud.Springboot2PostgresqlJpaHibernateCrudExampleApplication;
import net.guides.springboot2.crud.model.Comment;
import net.guides.springboot2.crud.model.Employee;
import net.guides.springboot2.crud.model.Post;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Springboot2PostgresqlJpaHibernateCrudExampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PostControllerTest {

	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@LocalServerPort
	private int port;
	
	private String getRootUrl() {
		return "http://localhost:" + port;
	}
	
	@Test
	void testGetAllPosts() {
		HttpHeaders httpHeaders = new HttpHeaders();
		HttpEntity<String> httpEntity = new HttpEntity<String>(null, httpHeaders);
		// org.springframework.boot.test.web.client.TestRestTemplate.exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<String> responseType, Object... urlVariables) throws RestClientException
		ResponseEntity<String> response = testRestTemplate.exchange(getRootUrl() + "/api/v1/posts", HttpMethod.GET, httpEntity, String.class);
		System.out.println(response.getBody());
		assertNotNull(response.getBody());
	}
	
	@Test
	void testCreatePost() {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String now = simpleFormat.format(date);
		
		Post post = new Post();
		post.setTitle("[JUNIT] title : " + now);
		post.setDescription("[JUNIT] description : " + now);
		
		ResponseEntity<Post> postResponse = testRestTemplate.postForEntity(getRootUrl() + "/api/v1/posts", post, Post.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
		
		System.out.println(postResponse.getBody());
		assertEquals("[JUNIT] title : " + now, postResponse.getBody().getTitle());
		assertEquals("[JUNIT] description : " + now, postResponse.getBody().getDescription());
	}
	
	@Test
	void testCreatePostWithComments() {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String now = simpleFormat.format(date);
		
		Comment comment1 = new Comment("[JUNIT] text1 : " + now);
		Comment comment2 = new Comment("[JUNIT] text2 : " + now);
		Comment comment3 = new Comment("[JUNIT] text3 : " + now);
		
		
		Post post = new Post();
		post.setTitle("[JUNIT] title : " + now);
		post.setDescription("[JUNIT] description : " + now);
		post.getComments().add(comment1);
		post.getComments().add(comment2);
		post.getComments().add(comment3);
		
		ResponseEntity<Post> postResponse = testRestTemplate.postForEntity(getRootUrl() + "/api/v1/posts", post, Post.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
		
		System.out.println(postResponse.getBody());
		assertEquals("[JUNIT] title : " + now, postResponse.getBody().getTitle());
		assertEquals("[JUNIT] description : " + now, postResponse.getBody().getDescription());
	}
}
