package net.guides.springboot2.crud.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;

import net.guides.springboot2.crud.Springboot2PostgresqlJpaHibernateCrudExampleApplication;
import net.guides.springboot2.crud.model.Employee;
//import org.springframework.test.context.junit4.SpringRunner;

//@RunWith(SpringRunner.class) //Junit4
@ExtendWith(SpringExtension.class) // junit-jupiter-5.5.2, junit-jupiter-api-5.5.2
@SpringBootTest(classes = Springboot2PostgresqlJpaHibernateCrudExampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeControllerTest {

	@Autowired
	private TestRestTemplate testRestTemplate;
	
	@LocalServerPort
	private int port;
	
	private String getRootUrl() {
		return "http://localhost:" + port;
	}
	
	@Test
	public void contextLoads() {
		System.out.println(getRootUrl());
	}
	
	@Test
	public void testGetAllEmployees() {
		HttpHeaders httpHeaders = new HttpHeaders();
		HttpEntity<String> httpEntity = new HttpEntity<String>(null, httpHeaders);
		// org.springframework.boot.test.web.client.TestRestTemplate.exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<String> responseType, Object... urlVariables) throws RestClientException
		ResponseEntity<String> response = testRestTemplate.exchange(getRootUrl() + "/api/v1/employees", HttpMethod.GET, httpEntity, String.class);
		System.out.println(response.getBody());
		assertNotNull(response.getBody());
	}

	@Test
	void testGetEmployee() {
        Employee employee = testRestTemplate.getForObject(getRootUrl() + "/api/v1/employees/1", Employee.class);
        System.out.println(employee);
        assertNotNull(employee);
	}

	@Test
	void testCreateEmployee() {
		Employee employee = new Employee();
		employee.setEmailId("admin3@gmail.com");
		employee.setFirstName("admin3");
		employee.setLastName("AdminFamily3");
		
		ResponseEntity<Employee> postResponse = testRestTemplate.postForEntity(getRootUrl() + "/api/v1/employees", employee, Employee.class);
		
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
		
		System.out.println(postResponse.getBody());
		
		assertEquals("admin@gmail.com", postResponse.getBody().getEmailId());
		assertEquals("admin", postResponse.getBody().getFirstName());
		assertEquals("AdminFamily", postResponse.getBody().getLastName());
	}

	@Test
	void testUpdateEmployee() {
		int id = 3;
		// get original employee's information
        Employee employee = testRestTemplate.getForObject(getRootUrl() + "/api/v1/employees/" + id, Employee.class);
        employee.setFirstName("admin1");
        employee.setLastName("AdminFamily2");
        
        // update original employee's information
        testRestTemplate.put(getRootUrl() + "/api/v1/employees/" + id, employee);
        
        // get updated employee's information
        Employee updatedEmployee = testRestTemplate.getForObject(getRootUrl() + "/api/v1/employees/" + id, Employee.class);
        System.out.println(updatedEmployee);
        
        assertNotNull(updatedEmployee);
	}

	@Test
	void testDeleteEmployee() {
		int id = 3;
        Employee employee = testRestTemplate.getForObject(getRootUrl() + "/api/v1/employees/" + id, Employee.class);
        assertNotNull(employee);
        testRestTemplate.delete(getRootUrl() + "/api/v1/employees/" + id);
        try {
             employee = testRestTemplate.getForObject(getRootUrl() + "/api/v1/employees/" + id, Employee.class);
        } catch (final HttpClientErrorException e) {
             assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
	}

}
